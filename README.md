This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## OL Software - Prueba Desarrollador  Front-end 

###Requisitos previos para el funcionamiento del proyecto
* Para el funcionamiento óptimo del proyecto se necesita tener instalados las siguientes dependencias y librerías:

1. Node js versión lts
2. npm

##Enlace al repositorio

A continuación se adjunta el enlace al repositorio de Bitbucket para poder clonar y descargarlo:

```git
git clone https://AlejoMartinez85@bitbucket.org/AlejoMartinez85/frontend-test.git
```

El enlace anterior se pega en la consola especificando la ruta en la se quiere guardar el repositorio

### `Instalación`

Abrir el proyecto desde la terminal  escogida para instalar todas las dependencias necesarias para correr el proyecto adecuadamente.

```console
npm i
```

Una vez termine la instalación de paquetes y dependencias procedemos a ingresar el siguiente comando para  correr el proyecto

```js
npm start
```

Una vez termine la montada del servidor automaticamente el proyecto abriría una ventana en el navegador por defecto con la ruta **localhost:3000**.

### Credenciales para el acceso al portal web

Para esta prueba se le asigno el usuario de con la siguientes credenciales:

`usuario: luis.gonzales@olsoftware.com
`
`clave: 123456789`

##Funcionalidades

En el portal web se podrán realizar las siguientes acciones: 

* Realizar Inicio de sesión
* Realizar cierre de sesión

* **Usuarios**
	* Consultar usuarios existentes 
	* Crear usuario
	* Editar usuarios
	* Eliminar usuarios
* **Roles**
	* Consultar roles existentes 	
	* Crear roles
	* Editar roles
	* Eliminar roles
* **Estados**
	* Consultar Estados existentes
	* Crear estados
	* Editar estados
	* Eliminar estados 	

Cabe resaltar que al momento de la edición o eliminación de estados y roles el sistema actualizará a los usuarios con nuevos estados y roles para reemplazar los eliminados anteriormente. 	


### `npm run build` Creación del build

### Enlace al portal web

También podemos acceder al portal web por medio del siguiente enlace:
[portalweb](https://prueba-recae-js--udemy.web.app)
