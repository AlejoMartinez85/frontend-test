/** import rxjs */
import {Subject} from 'rxjs';

/** for user created */
export const userCreated = new Subject();
export const roleCreated = new Subject();
export const stateCreated = new Subject();
export const userFilter = new Subject();


/** valida si hay una sesión activa */
export const validateSession = () => {
    if (localStorage.getItem('uid') != null) {
        return true
    } else {
        return false;
    }
}

export const validateUserFields = (obj) => {
    if (!obj.name.trim()) {
        return { state: false, msg: 'El nombre no puede ir vacio' }
    }
    if (!obj.surname.trim()) {
        return { state: false, msg: 'Los apellidos no puede ir vacio' }
    }
    if (!obj.document.trim()) {
        return { state: false, msg: 'El documento no puede ir vacio' }
    }
    if (!obj.role.trim()) {
        return { state: false, msg: 'El rol no puede ir vacio' }
    }
    if (!obj.stateValue.trim()) {
        return { state: false, msg: 'El estado no puede ir vacio' }
    }
    if (!obj.phone.trim()) {
        return { state: false, msg: 'El teléfono no puede ir vacio' }
    }
    return {state: true, msg: ''}
}