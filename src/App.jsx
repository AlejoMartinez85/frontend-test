/** core */
import React from 'react';
/** theme provider */
import {ThemeProvider} from '@material-ui/core/styles'
/** routes */
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
/** styles */
import './App.css';
/** components */
import Login from './components/login/Login';
import Dashboard from './components/dashboard/Dashboard';
import SideBar from './components/sideBar/SideBar';
/** import theme rules */
import theme from './themeConfig'; 


function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          {/* Define routes */}
          <Route path="/" exact>
            <Login/>
          </Route>
          <Route path="/dashboard">
            <Dashboard/>
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
