import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
/** declare firebase var  with configurations */
const firebaseConfig = {
    apiKey: "AIzaSyDEkDOV75JNv_4RfF837GQV8ZU0D7SHXzc",
    authDomain: "prueba-recae-js--udemy.firebaseapp.com",
    databaseURL: "https://prueba-recae-js--udemy.firebaseio.com",
    projectId: "prueba-recae-js--udemy",
    storageBucket: "prueba-recae-js--udemy.appspot.com",
    messagingSenderId: "48722902385",
    appId: "1:48722902385:web:99e0e9d48a3aeacd154ad0",
    measurementId: "G-2LTNW6TBHZ"
};
/** Initialize Firebase */
firebase.initializeApp(firebaseConfig);
/** declase utils for firebase */
const db = firebase.firestore();
const auth = firebase.auth();
/** export utils for global use */
export {
    db,
    auth
};