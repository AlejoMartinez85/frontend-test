import React from 'react';
/** Cards */
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
/** css */
import { stylesCss } from '../../../styles.css';
/** components */
import CreateState from '../createState/CreateState';
import StatesTables from '../statesTable/StatesTables';

const States = () => {
    return (
        <div className="w-100 fullHeight">
            {/* <div className="container"> */}
            <div className="row justify-content-center">
                <div className="col-12 col-xs-12 col-xl-10 px-0">
                    <Card>
                        <CardContent>
                            {/* create user component */}
                            <CreateState />
                            <div className="mb-4"></div>
                            {/* tableUser Component */}
                            <StatesTables />
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default States
