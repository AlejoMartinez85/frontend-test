import React from 'react';
/** Core Material UI */
import { makeStyles, Drawer, Typography, Avatar } from '@material-ui/core';
import { withStyles} from '@material-ui/core/styles';
/** component */
import Divider from '@material-ui/core/Divider';
/** List */
import ListItems from '../listItems/ListItems';
/** css */
import { stylesCss } from '../../styles.css';
/** badge */
import Badge from '@material-ui/core/Badge';


/** const for width */
const drawerWidth = 240;
/** classes */
const styles = makeStyles(theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink:0
    },
    drawerPaper: {
        width:drawerWidth,
        background: 'linear-gradient(to bottom, rgba(100, 181, 246, 1) 0%, rgba(33, 149, 243, 1) 45%, rgba(25, 117, 210, 1) 71%, rgba(13, 72, 161, 1) 100%)'

    },
    toolbar: theme.mixins.toolbar,
    whiteDivider: {
        color: '#ffffff',
        background: '#ffffff'
    },
    white: {
        color:'white'
    }
}));

const StyledBadge = withStyles(theme => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
    '0%': {
        transform: 'scale(.8)',
        opacity: 1,
    },
    '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
    },
},
}))(Badge);

const image = '../../images/avatar.png';

const SideBar = (props) => {
    /** my styles  var */
    const classes = styles();
    return (
        <Drawer 
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper
            }}
            /** se le hacen binding al variant del drawer */
            variant={props.variant}
            open={props.open}
            onClose={props.onClose ? props.onClose : null}
            anchor="left">

            <div className={classes.toolbar}>
                <div className="row w-100 justify-content-center align-items-center mt-3">
                    <div className="mr-4">
                        <StyledBadge
                            overlap="circle"
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            variant="dot">
                            <Avatar alt="Remy Sharp" src={image} />
                        </StyledBadge>
                    </div>
                    <div>
                        <Typography
                            className={`${classes.white}`}
                            variant='h6'>
                            OLSoftware
                        </Typography>
                    </div>
                </div>
            </div>
            <Divider classes={{
                root: classes.whiteDivider
            }} />
            {/* import listItems */}
            <ListItems />
        </Drawer>
    )
}

export default SideBar
