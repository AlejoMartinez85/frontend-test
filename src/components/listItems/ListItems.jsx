import React from 'react';
/**List */
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
/** css */
import { stylesCss } from '../../styles.css';
/** icon */
import FaceIcon from '@material-ui/icons/Face';
import MapIcon from '@material-ui/icons/Map';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import BuildIcon from '@material-ui/icons/Build';
import {
    NavLink,
    useRouteMatch
} from "react-router-dom";

const ListItems = () => {
    let { url } = useRouteMatch();
    return (
        <List component="nav">
            <NavLink to={`${url}/programming`} exact>
                <ListItem button className="mb-4">
                    <ListItemIcon>
                        <MapIcon
                        style={{color: '#ffffff', fontSize: 30}} />
                    </ListItemIcon>
                    <ListItemText
                        className="white">
                        Programación
                    </ListItemText>
                </ListItem>
            </NavLink>
            <NavLink to={`${url}/states`} exact>
                <ListItem button className="mb-4">
                    <ListItemIcon>
                        <BuildIcon
                        style={{color: '#ffffff', fontSize: 30}} />
                    </ListItemIcon>
                    <ListItemText
                        className="white">
                        Estados
                    </ListItemText>
                </ListItem>
            </NavLink>
            <NavLink to={`${url}/profiles`} >
                <ListItem button className="mb-4">
                    <ListItemIcon
                    style={{color: '#ffffff', fontSize: 30}}>
                        <FormatListBulletedIcon />
                    </ListItemIcon>
                    <ListItemText className="white">
                        Perfiles
                    </ListItemText>
                </ListItem>
            </NavLink>
            <NavLink to={`${url}/roles`} >
                <ListItem button className="mb-4">
                    <ListItemIcon>
                        <AssignmentIndIcon
                        style={{color: '#ffffff', fontSize: 30}} />
                    </ListItemIcon>
                    <ListItemText className="white">
                        Roles
                    </ListItemText>
                </ListItem>
            </NavLink>
            <NavLink to={`${url}/users`} >
                <ListItem button className="mb-4">
                    <ListItemIcon>
                        <FaceIcon
                        style={{color: '#ffffff', fontSize: 30}} />
                    </ListItemIcon>
                    <ListItemText className="white">
                        Usuarios
                    </ListItemText>
                </ListItem>
            </NavLink>
        </List>
    )
}

export default ListItems
