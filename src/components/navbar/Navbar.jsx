import React from 'react';
/** firebase */
import { auth } from '../../firebase';
/** router */
import { withRouter } from 'react-router-dom';
/** navbar */
import { AppBar, Toolbar, Typography, makeStyles, IconButton, Hidden } from '@material-ui/core';

/** icons */
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircle from '@material-ui/icons/AccountCircle';

/** width const **/
const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    title: {
        flexGrow: 1
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        backgroundColor: '#ffffff'
    },
    black: {
        color:'#212121'
    },
    grey: {
        color: '#616161'
    }
}))

const Navbar = (props) => {
    const [email, setEmatil] = React.useState('');
    /** logOut */
    const logOut = () => {
        auth.signOut().then(() => {
            localStorage.clear();
            /** send to login */
            props.history.push('/');
        });
    };

    React.useEffect(() => {
        const test = () => {
            setEmatil(localStorage.getItem('email'));
        }

        test();
    }, []);
    /** styles */
    const classes = useStyles();
    return (
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="primary"
                        aria-label="menu"
                        className={classes.menuButton}
                        onClick={() => props.openSideMenu()}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography 
                        color="primary"
                        variant='h6' 
                        className={classes.title}>
                        Prueba Front-end
                    </Typography>
                    {/* vamos a esconder el icono y el
                    nombre del usuario desde md para abajo */}
                    <Hidden smDown>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            className={classes.grey}
                        >
                            <AccountCircle />
                        </IconButton>
                        <Typography
                            className={classes.black}
                            variant='h6'>
                            {email}
                        </Typography>
                    </Hidden>
                    <IconButton
                         className="ml-5"
                        onClick={() => logOut()}
                        color="primary"
                        aria-label="menu">
                        <ExitToAppIcon/>    
                    </IconButton>
                </Toolbar>
            </AppBar> 
    )
}

export default withRouter(Navbar)
