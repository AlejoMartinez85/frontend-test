import React from 'react';
/** navbar */
import Navbar from '../navbar/Navbar';
/** styles */
import {makeStyles, Hidden} from '@material-ui/core';
import theme from '../../themeConfig';
import SideBar from '../sideBar/SideBar';
/** components */
import Users from '../UserComponent/users/Users';
import Profiles from '../profiles/Profiles';
import States from '../StatesComponent/states/States'
import Programming from '../programming/Programming';
import Roles from '../RolesComponent/Roles/Roles';
/** router */
import { withRouter } from 'react-router-dom';
import {
    Switch,
    Route,
    useRouteMatch
} from "react-router-dom";

/** Utils */
import { validateSession} from '../../utils/Utils';

const styles = makeStyles(themes => ({
    /** root clase for all */
    root: {
        display: 'flex'
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow:1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
        height:'100%'
    }
}));

const Dashboard = (props) => {
    let { url } = useRouteMatch();
    /** declare my class */
    const classes = styles();
    /** usermenu state */
    const [userMenu, setUserMenu] = React.useState(false);
    /** toggle function menu */
    const handleToggleMenu = () => {
        setUserMenu(!userMenu);
    };
    /** useEffect */
    React.useEffect(() => {
        /** preguntar si hay un usuario existente */
        const isUserActive = () => {
            const activeUser = validateSession();
            if (!activeUser) {
                props.history.push('/');    
            }
        };
        /** call isUserActive */
        isUserActive();
        // getDataUser();

    }, [props.history]);
    return (
        <div className={classes.root}>
            {/* import navbar */}
            <Navbar
                openSideMenu={handleToggleMenu}
             />
            {/* envolvemos el sidebar en un hidden para 
            saber cuando mostrarlo - PERMANENTE */}
            <Hidden xsDown>
                {/* sideBar */}
                <SideBar
                /** se mandan las configuraciones
                 * para que el sidebar sepa como
                 * comportarse
                 */
                variant = "permanent"
                /** se manda el open para avisar cuando se 
                 * debe de abrir */
                open={true}
                />
            </Hidden>
            {/* envolvemos el sidebar en un hidden para 
            saber cuando mostrarlo - TEMPORAL */}
            <Hidden smUp>
                {/* sideBar */}
                <SideBar
                /** se mandan las configuraciones
                 * para que el sidebar sepa como
                 * comportarse
                 */
                variant = "temporary"
                /** se manda el open para avisar cuando se 
                 * debe de abrir */
                open={userMenu}
                onClose={handleToggleMenu}
                />
            </Hidden>
            {/* content data */}
            <div className={classes.content}>
                <div className={classes.toolbar}></div>
                <Switch>
                    {/* crud route */}
                    <Route path={`${url}/users`}>
                        <Users/>
                    </Route>
                    {/* profiles route */}
                    <Route path={`${url}/profiles`}>
                       <Profiles/>
                    </Route>
                    {/* profiles route */}
                    <Route path={`${url}/roles`}>
                       <Roles/>
                    </Route>
                    {/* profiles route */}
                    <Route path={`${url}/programming`}>
                       <Programming/>
                    </Route>
                    {/* profiles route */}
                    <Route path={`${url}/states`}>
                       <States/>
                    </Route>
                </Switch>
            </div>
        </div>
    )
}

export default withRouter(Dashboard)
