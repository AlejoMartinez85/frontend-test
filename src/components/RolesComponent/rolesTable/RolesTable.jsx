import React from 'react';
/** Firebase */
import { db } from '../../../firebase';
/** subject */
import { roleCreated } from '../../../utils/Utils';
/** styles */
import { makeStyles, Typography, Button } from '@material-ui/core';
/** table */
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
/** button */
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
/** modal */
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
/** toast */
import Snackbar from '@material-ui/core/Snackbar';
/** text fields */
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
/** spinner */
import CircularProgress from '@material-ui/core/CircularProgress';
/** skeleton */
import Skeleton from '@material-ui/lab/Skeleton';

/** styles for modal and table */
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 2),
        borderRadius: 20
    },
    borderR: {
        borderRadius: '20'
    },
    table: {
        // minWidth: 650,
    },
    inputDisabled: {
        backgroundColor: '#E8E8E8'
    },
    warning: {
        color: '#ff9100'
    }
}));

const RolesTable = () => {
    /** Styles */
    const classes = useStyles();
    /** state for users */
    const [uid, setUid] = React.useState('');
    /** state for users */
    const [roles, setRoles] = React.useState([]);
    /** state for modal */
    const [openModal, setOpenModal] = React.useState(false);
    /** state for modal */
    const [showSpinner, setShowSpinner] = React.useState(false);
    /** state for modal delete user */
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    /** declare name */
    const [name, setName] = React.useState('');
    /** declare value */
    const [value, setValue] = React.useState('');
    /** toast state */
    const [openToast, setOpenToast] = React.useState(false);
    /** toast state */
    const [messageToast, setMessageToast] = React.useState('Rol Actualizado con éxito!');


    React.useEffect(() => {
        /** pending function of new users  */
        const observerNewRolCreated = () => {
            roleCreated.subscribe((data) => {

                if (data) {
                    const newRole = data.newRole;
                    setRoles(roles => [
                        ...roles, newRole
                    ]);
                    // console.log('newRole: ', newRole);
                    // console.log('roles: ', roles);
                }
            });
        };

        /** get roles from firebase */
        const getRoles = async () => {
            try {
                const response = await db.collection('Roles').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setRoles(arr);
                // console.log(arr);
            } catch (error) {
                console.log('@errorGetRoles: ', error);

            }
        };
        /** call function */
        observerNewRolCreated();
        getRoles();
    },[]);

    /** función que envia la información para editarlo */
    const editRole = React.useCallback(async () => {
        try {
            setShowSpinner(true);
            const roleEdit = {
                name,
                value
            }
            await db.collection('Roles').doc(uid).update(roleEdit);
            const rolesEditArr = roles.map(item => (
                item.id === uid ? roleEdit : item
            ));
            setRoles(rolesEditArr);
            /** show toas */
            handleShowToast();
            setShowSpinner(false);
            handleClose();
            /** reset values */
            resetValues();
        } catch (error) {
            console.log('@editRoleError: ', error);
            setShowSpinner(false);
            setMessageToast('Ups, ha ocurrido un error');
            handleShowToast();

        }
    }, [name, value, uid]);

    /** función que elimina al rol seleccionado */
    const deleteRol = React.useCallback(async () => {
        try {
            setShowSpinner(true);
            await db.collection('Roles').doc(uid).delete();
            /** filtramos el arreglo sin el usuario eliminado */
            const newEditArr = roles.filter(item => item.id !== uid);
            /** ahora vamos a consumir el servicio de trae todos los usuarios y
             * vamos a cambiarles el rol por el rol por defecto
             */
            await getUserAndDeteleActualRol();
            /** */
            setRoles(newEditArr);
            setMessageToast('Rol eliminado!');
            handleShowToast();
            setShowSpinner(false);
            handleCloseDeleteMOdal();
        } catch (error) {
            console.log('@deleteRolError: ', error);
            setMessageToast('No se ha podido borrar el rol');
            setShowSpinner(false);
            handleShowToast();
        }
    });

    /** función que trae a los usuarios, busca el rol que se elimino
     * y lo reemplaza por el por defecto 'Usuario'
     */
    const getUserAndDeteleActualRol = React.useCallback(async () =>{
        try {
            const response = await db.collection('Users').get();
            const arr = response.docs.map(doc => doc.data());
            /** procede a traer los usuarios, filtrar los que tengan el rol y cambiarlo por el por defecto de usuario */
            arr.map(user => {
                if (user.role == value) {
                    user.role = '70';
                    user.roleName = 'Usuario'
                }
            });
            arr.map(async(user) => (
                await db.collection('Users').doc(user.uid).update(user)
            ));
        } catch (error) {
            console.log('@errorGetUsers: ', error);

        }
    });

    /** process edit onsubmit form */
    const editProcessData = (event) => {
        event.preventDefault();
        /** validamos que los campos esten llenos */
        editRole();
    };
    /** setea la información del rol seleccionado */
    const pathRoleInformation = (id) => {
        setUid(id);
        const rolSelected = roles.filter(rol => rol.id === id);
        setName(rolSelected[0].name);
        setValue(rolSelected[0].value);
    };

    /** -------- funciones para abrir y cerrar los modales ------ */
    /** declare var for modal */
    const handleShow = (uid) => {
        /** seteamos el uid del usuario seleccionado */
        setOpenModal(true);
        pathRoleInformation(uid);
    };
    /** cierra el modal de actualizar usuarios */
    const handleClose = () => setOpenModal(false);
    /** declare var for modal */
    const handleShowDeleteModal = (uid, name, value) => {
        /** seteamos el uid del usuario seleccionado */
        setUid(uid);
        setName(name);
        setValue(value);
        setOpenDeleteModal(true);
    };
    /** cierra el modal de eliminar usuarios */
    const handleCloseDeleteMOdal = () => {
        setName('');
        setUid('');
        setValue('');
        setOpenDeleteModal(false);
    };
    /** declare var for toast */
    const handleShowToast = () => setOpenToast(true);
    const handleCloseToast = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenToast(false);
    };
    /** reset form values */
    const resetValues = () => {
        setName('');
        setValue('');
    };

    return (
        <div>
            {
                roles.length == 0 ? (
                <div className="p-4">
                    <div className="p-4">
                        <Skeleton height={40} />
                        <Skeleton animation={false} height={20} />
                        <Skeleton animation="wave" className="mb-3" height={30} />
                        <Skeleton />
                        <Skeleton animation={false} height={20} />
                        <Skeleton animation="wave" className="mb-3" height={30} />
                        <Skeleton />
                        <Skeleton animation={false} height={20} />
                        <Skeleton animation="wave" className="mb-3" height={30} />
                        <Skeleton />
                        <Skeleton animation={false} height={20} />
                        <Skeleton animation="wave" className="mb-3" height={30} />
                    </div> 
                </div>
                ) : (
                        <TableContainer >
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>
                                            <Typography
                                                variant="h6">
                                                Nombre
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Valor
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Acción
                                </Typography>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {roles.map((rol) => (
                                        <TableRow key={rol.id}>
                                            <TableCell component="th" scope="row">
                                                {rol.name}
                                            </TableCell>
                                            <TableCell align="center">{rol.value}</TableCell>
                                            <TableCell align="center">
                                                <IconButton
                                                    disabled={rol.value == '70' ? true : false}
                                                    className="pr-0"
                                                    style={{ color: rol.value == '70' ? '#9e9e9e' : '#4a4a4a' }}
                                                    onClick={() => handleShow(rol.id)}>
                                                    <CreateIcon />
                                                </IconButton>
                                                <IconButton
                                                    disabled={rol.value == '70' ? true : false}
                                                    className="pr-0 mr-3"
                                                    style={{ color: rol.value == '70' ? '#9e9e9e' : '#4a4a4a' }}
                                                    onClick={() => handleShowDeleteModal(rol.id, rol.name, rol.value)}>
                                                    <DeleteIcon />
                                                </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                )
            }

            {/* Modal containter for edit rol */}
            <Modal
                className={classes.borderR}
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableAutoFocus={true}
                disableEnforceFocus={true}>
                <Fade in={openModal}>
                    <div className={classes.paper}>
                        <div className="container p-4">
                            <Typography variant="h5"
                                className="mb-4 " color="primary">Editar rol</Typography>
                            <div className="row justify-content-center">
                                <form onSubmit={editProcessData}
                                    className="px-4 col-10">
                                    <div className="row">
                                        <div className="col-sm-12 col-xs-12 col-md-12 col-lx-12">
                                            <InputLabel>Nombre</InputLabel>
                                            <TextField
                                                value={name}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="text"
                                                placeholder="Escribir..."
                                                onChange={event => setName(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-12 d-flex justify-content-center px-0">
                                            {
                                                showSpinner ? (
                                                    <CircularProgress color="primary" />
                                                ) : (
                                                        <button
                                                            type="submit"
                                                            className="btn btn-sm btn-hover color-5 px-4">
                                                            Actualizar
                                                        </button>
                                                    )
                                            }
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Fade>
            </Modal>

            {/* Modal for delete rol */}
            <Modal
                className={classes.borderR}
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openDeleteModal}
                onClose={handleCloseDeleteMOdal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                disableAutoFocus={true}
                disableEnforceFocus={true}>

                <Fade in={openDeleteModal}>
                    <div className={classes.paper}>
                        <div className="container p-4 text-center">
                            {
                                showSpinner ? (
                                    <CircularProgress color="primary" />
                                ) : (
                                        <WarningIcon
                                            className="mb-3"
                                            style={{ color: '#ff9100', fontSize: 100 }}
                                        />
                                    )
                            }

                            <h3
                                className={`${classes.warning} text-center mb-4`}>
                                ¡Ups ten cuidado!
                                </h3>
                            <p>¿Estas seguro de eliminar el rol de {name}?</p>

                            <div className="row">
                                <div className="col-6 text-center">
                                    <button
                                        className="btn btn-lg btn-hover color-11"
                                        onClick={deleteRol}>
                                        Eliminar
                                    </button>
                                </div>
                                <div className="col-6 text-center">
                                    <button
                                        className="btn btn-lg btn-hover color-9"
                                        onClick={handleCloseDeleteMOdal}>
                                        Cancelar
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </Fade>
            </Modal>
            {/* Toast */}
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={openToast}
                autoHideDuration={6000}
                onClose={handleCloseToast}
                message={messageToast}
                action={
                    <React.Fragment>
                        <Button color="secondary" size="small" onClick={handleCloseToast}>
                            Cerrar
                        </Button>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseToast}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    )
}

export default RolesTable
