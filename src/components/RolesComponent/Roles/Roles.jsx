import React from 'react';
/** Cards */
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
/** css */
import { stylesCss } from '../../../styles.css';

/** components */
import CreateRoles from '../createRoles/CreateRoles';
import RolesTable from '../rolesTable/RolesTable';


const Roles = () => {

    return (
        <div className="w-100 fullHeight">
            {/* <div className="container"> */}
            <div className="row justify-content-center">
                <div className="col-12 col-xs-12 col-xl-10 px-0">
                    <Card>
                        <CardContent>
                            {/* create user component */}
                            <CreateRoles />
                            <div className="mb-4"></div>
                            {/* tableUser Component */}
                            <RolesTable />
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default Roles
