import React from 'react';
/** styles */
import { Typography, makeStyles, Button } from '@material-ui/core';
/** text fields */
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
/** css */
import { stylesCss } from '../../../styles.css';
/** icon */
import IconButton from "@material-ui/core/IconButton";
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import CloseIcon from '@material-ui/icons/Close';
/** Firebase */
import { db } from '../../../firebase';
/** modal */
// import Modal from 'react-bootstrap/Modal';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
/** subject */
import { roleCreated } from '../../../utils/Utils';
/** toast */
import Snackbar from '@material-ui/core/Snackbar';

/** styles for modal */
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 2),
        borderRadius: 20
    },
    borderR: {
        borderRadius: '20'
    }
}));

const CreateRoles = () => {
    /** asign modal classes */
    const classes = useStyles();
    /** state for modal */
    const [openModal, setOpenModal] = React.useState(false);
    /** declare name */
    const [name, setName] = React.useState('');
    /** toast state */
    const [openToast, setOpenToast] = React.useState(false);
    /** toast state */
    const [messageToast, setMessageToast] = React.useState('Rol creado con éxito!');
    /** declare var for states */
    const handleShowToast = () => setOpenToast(true);
    const handleCloseToast = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenToast(false);
    };
    /** declare var for modal */
    const handleClose = () => setOpenModal(false);
    const handleShow = () => setOpenModal(true);

    /** reset form values */
    const resetValues = () => {
        setName('');
    };

    /** procesa la data a registrar */
    const processRole = (event) => {
        event.preventDefault();
        if(!name.trim()) {
            setMessageToast('El nombre no puede ir vacio');
            handleShowToast();
            return;
        }
        createRole();
    }

    /** function register */
    const createRole = React.useCallback(async () => {
        try {
            /** relacionamos el uid del registro a la colección de 
             * usuarios
             */
            const newRole = {
                name,
                value: `${Math.floor(Math.random() * (100 - 1)) + 1}`
            };
            const response  = await db.collection('Roles').add(newRole);
            /** emitt nuew user to userTable component */
            console.log('response: ', response);
            newRole.id = response.id;
            roleCreated.next({
                newRole
            });
            /** show toas */
            handleShowToast();
            /** reset inputs values */
            resetValues();
        } catch (error) {
            console.error('@errorRegister,', error);
            setMessageToast('Ups, ha ocurrido un error');
        }
    }, [name]);

    return (
        <div className="row">
            {/* title and create roles button */} 
            <div className="col-12">
                <div className="row">
                    <div className="col-8 col-sm-8 col-xs-8 col-md-8 col-lx-8">
                        <div className="d-flex">
                            <AssignmentIndIcon
                                className="mr-2"
                                color="primary"
                                style={{ fontSize: 30 }} />
                            <Typography
                                variant="h6"
                                color="primary">
                                Roles existentes
                            </Typography>
                        </div>
                    </div>
                    {/* button for create user */}
                    <div className="col-4 col-sm-4 col-xs-4 col-md-4 col-lx-4">
                        <div className="text-right">
                            <button
                                className="btn btn-hover color-9"
                                onClick={handleShow}>
                                Crear rol
                            </button> 
                        </div>
                    </div>
                </div>
            </div>
            {/* modal component */}
            <Modal
                className={classes.borderR}
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableAutoFocus={true}
                disableEnforceFocus={true}>
                <Fade in={openModal}>
                    <div className={classes.paper}>
                        <div className="container p-4">
                            <Typography variant="h5"
                                className="mb-4" color="primary">Crear rol</Typography>
                            <div className="row justify-content-center">
                                <form onSubmit={processRole}
                                    className="px-4 col-10">
                                    <div className="row justify-content-center">
                                        <div className="col-sm-12 col-xs-12 col-md-12 col-lx-12">
                                            <InputLabel>Nombre</InputLabel>
                                            <TextField
                                                value={name}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="text"
                                                placeholder="Escribir..."
                                                onChange={event => setName(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-8 d-flex justify-content-around px-0">
                                            <button
                                                type="submit"
                                                className="btn btn-sm btn-hover color-5 px-4">
                                                Crear
                                            </button>
                                            <button
                                                type="button"
                                                onClick={() => resetValues()}
                                                className="btn-linear-green px-4 btn btn-sm">
                                                Limpiar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>  
                </Fade>    
            </Modal>
            {/* Toast */}
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={openToast}
                autoHideDuration={6000}
                onClose={handleCloseToast}
                message={messageToast}
                action={
                    <React.Fragment>
                        <Button color="secondary" size="small" onClick={handleCloseToast}>
                            Cerrar
                        </Button>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseToast}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    )
}

export default CreateRoles
