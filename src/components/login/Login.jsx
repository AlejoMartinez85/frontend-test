import React from 'react'
/** material Ui */
/** Icons */
import IconButton from "@material-ui/core/IconButton";
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
/** text fields */
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
/** Cards */
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
/** styles */
import { makeStyles } from '@material-ui/core/styles';
/** Firebase */
import { auth, db } from '../../firebase';
/** router */
import { withRouter } from 'react-router-dom';
/** withWidth */
import {Hidden} from '@material-ui/core';
/** css */
import { stylesCss } from '../../styles.css';

/** We can inject some CSS into the DOM. */
/** card border-radius */
const cardStyle = makeStyles({
    cardRadius: {
        borderRadius: 20,
        padding: 20
    },
    white: {
        color:'#ffffff'
    }
})

const Login = (props) => {
    const classes = cardStyle();
    /** email state */
    const [email, setEmail] = React.useState('');
    /** passwordState */
    const [password, setPassword] = React.useState('');
    /** show password state */
    const [showPassword, setShowPassword] = React.useState(false);
    /** error */
    const [error, setError] = React.useState(null); 
    /** changeInputType */
    const [inputType, setInputType] = React.useState('password');

    /** get data from form */
    const prepareData = event => {
        event.preventDefault();
        /** get validators */
        validators();
        /** use login function */
        login();
    };
    /** login function */
    const login = React.useCallback(async() => {
        try {
            const response = await auth.signInWithEmailAndPassword(email, password);
            // console.log('@loginResponse: ', response);
            /** set uid in localstorage */
            /** consultamos al usuario para validar si el usuario es inactivo o activo */
            const res = await db.collection('Users').doc(response.user.uid).get();
            // console.log('res: ', res.data());
            if (!res.data().state) {
                localStorage.clear();
                handleError('suspended-account');
            } else {
                localStorage.setItem('uid', response.user.uid);
                localStorage.setItem('email', response.user.email);
                resetValue();
                props.history.push('/dashboard/users');
            }
            /** reset inputs values */
            /** get props */
        } catch (error) {
            console.error('@error.login: ', error)
            handleError(error.code);
        }
    }, [email, password, props.history]);
    /** handle error function */
    const handleError = (errorCode) => {
        switch (errorCode) {
            case 'auth/invalid-email':
                setError('El formato del correo ingresado es inválido.')
                break;
            case 'auth/email-already-in-use':
                setError('Este correo ya se encuentra en uso.');
                break;
            case 'auth/user-not-found':
                setError('Este correo no existe en la plataforma');
                break;
            case 'auth/wrong-password':
                setError('Contraseña incorrecta.');
                break;
            case 'suspended-account':
                setError('Cuenta suspendida.');
                break;
        };
    };
    /** validators function */
    const validators = () => {
        /** email validation */
        if (!email.trim()) {
            setError('Ingrese el email');
            return;
        }
        /** password validation */
        if (!password.trim()) {
            setError('Ingrese la contraseña');
            return;
        }
        if (password.length < 6) {
            setError('Ingrese en su contraseña más de 6 digitos');
            return;
        }
        /** hide error message */
        setError(null);
    }
    /** evita que el formulario recargue la página */
    const handleMouseDownPassword = (event) => {
        // event.preventDefault();
    };
    /** change showPassword state */
    const showValuePassword = () => {
        /** change showPassword value */
        setShowPassword(!showPassword);
        /** is validated to know what kind of input to put */
        if (!showPassword) {
            setInputType('text');
        } else {
            setInputType('password');
        }
    };
    /** reset inputs value */
    const resetValue = () => {
        setEmail('');
        setPassword('');
        setError(null);
    };

    /** verifico que hay un usuario */
    React.useEffect(() => {
        auth.onAuthStateChanged(user => {
            if (user && localStorage.getItem('uid') != undefined) {
                props.history.push('/dashboard/users');
            }
        });
    }, [props.history]);


    return (
        <div className="container-fluid pt-5 splash d-flex align-items-center">
            <div className="container mt-5">
                <div className="row justify-content-center">
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-8 col-lx-6 mb-5">
                        <Hidden lgUp>
                            <h2 className={`${classes.white} text-center text-shadow`}>Aplicación</h2>
                            <h2 className={`${classes.white} mb-3 text-center text-shadow`}>OLSoftware</h2>
                            <p className={`${classes.white} text-center text-shadow`}>Prueba práctica Front-end senior</p>
                        </Hidden>
                        <Hidden mdDown>
                            <h2 className={`${classes.white} text-shadow`}>Aplicación</h2>
                            <h2 className={`${classes.white} mb-3 text-shadow`}>OLSoftware</h2>
                            <p className={`${classes.white} text-shadow`}>Prueba práctica Front-end senior</p>
                        </Hidden>
                    </div>
                    <div className="col-xs-12 col-sm-10 col-md-6 col-lg-4 col-lx-6">
                        <Card className={classes.cardRadius}>
                            <CardContent className="row justify-content-center">
                                <h4 className={error ? 'mb-2' : 'mb-5'}>Inicio de sesión</h4>
                                {/* form start */}
                                <form onSubmit={prepareData}>
                                    {/* show error message */}
                                    {
                                        error && (
                                            <div className="col-12 alert alert-danger text-center">
                                                {error}
                                            </div>
                                        )
                                    }
                                    {/* Email input */}
                                    <div className=" px-0 col-12 mb-2 text-center">
                                        <TextField
                                            className="w-100"
                                            placeholder="Correo"
                                            variant="outlined"
                                            type="email"
                                            onChange={event => setEmail(event.target.value)}
                                            value={email} 
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="start">
                                                        <PermIdentityIcon
                                                            style={{color: '#4a4a4a'}} 
                                                        />
                                                    </InputAdornment>
                                                ),
                                            }}/>
                                    </div>
                                    {/* Password input */}
                                    <div className=" px-0 col-12 text-center mb-4">
                                        <TextField
                                            className="w-100"
                                            placeholder="Contraseña"
                                            variant="outlined"
                                            type={inputType}
                                            onChange={event => setPassword(event.target.value)}
                                            value={password}
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="start">
                                                        <IconButton
                                                            className="pr-0"
                                                            style={{color: '#4a4a4a'}}
                                                            onMouseDown={() => handleMouseDownPassword()}
                                                            onClick={() => showValuePassword()}>
                                                            {
                                                                showPassword ? (<Visibility/>) : (<VisibilityOff/>)
                                                            }
                                                        </IconButton>
                                                    </InputAdornment>
                                                )
                                            }}
                                        />
                                    </div>

                                    <div className="col-12 px-0">
                                        <button className="btn btn-primary btn-block btn-lg btn-hover color-9">
                                            Inicio de sesión
                                        </button>
                                    </div>
                                </form>
                                {/* form ends */}
                            </CardContent>
                        </Card>
                    </div>
                </div>
                {/* <Typography>
                   Ancho: {props.width}
                </Typography> */}
            </div>
        </div>
    )
}

export default withRouter(Login)
