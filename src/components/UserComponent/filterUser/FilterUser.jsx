/** core */
import React from 'react';
/** Firebase */
import { db } from '../../../firebase';
/** Cards */
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
/** styles */
import { Typography, IconButton } from '@material-ui/core';
/** text fields */
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
/** icon */
import PersonIcon from '@material-ui/icons/Person';
/** select */
import { Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
/** utild */
import { userFilter} from '../../../utils/Utils';

const FilterUser = () => {
    /** declare name */
    const [name, setName] = React.useState(undefined);
    /** declare surname */
    const [surname, setSurname] = React.useState(undefined);
    /** declare document */
    const [document, setDocument] = React.useState(undefined);
    /** declare rol  */
    const [role, setRole] = React.useState(undefined);
    /** declare state  */
    const [state, setState] = React.useState(undefined);
    /** declare phone  */
    const [phone, setPhone] = React.useState(undefined);
    /** declare email  */
    const [email, setEmail] = React.useState(undefined);
    /** declare Roles Arr  */
    const [rolesArr, setRolesArr] = React.useState([]);
    /** declare state Arr */
    const [stateArr, setStateArr] = React.useState([]);
    /** setRole value */
    const handleRoleValue = event => {
        setRole(event);
    }
    /** set state value */
    const handleStateValue = event => {
        setState(event);
    }

    React.useEffect(() => {

        /** get roles from firebase */
        const getRoles = async () => {
            try {
                const response = await db.collection('Roles').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setRolesArr(arr);
                // console.log(arr);
            } catch (error) {
                console.log('@errorGetRoles: ', error);
            }
        };
        /** get states from firebase */
        const getStates = async () => {
            try {
                const response = await db.collection('State').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setStateArr(arr);
                // console.log('states: ',arr);
            } catch (error) {
                console.log('@errorGetStates: ', error);

            }
        };
        /** call function */
        getRoles();
        getStates();
    }, []);
    /** process register */
    const filter = (event) => {
        event.preventDefault();
        /** validamos que los campos esten llenos */
        const obj = {
            name,
            surname,
            document,
            role,
            state,
            phone,
            email
        };
        userFilter.next({
            filter: obj
        });
    };

    const resetValues = () => {
        setName(undefined);
        setSurname(undefined);
        setDocument(undefined);
        setRole(undefined);
        setState(undefined);
        setPhone(undefined);
        setEmail(undefined);
    }

    return (
        <Card className="p-3">
            <CardContent className="row justify-content-center">
                <div className="col-12 px-0 mb-3 d-flex justify-content-center">
                    <IconButton
                        className="p-0 mr-2"
                        color="primary">
                        <PersonIcon />
                    </IconButton>
                    <Typography
                        color="primary"
                        variant='h6'>
                        Filtrar búsqueda
                                </Typography>
                </div>
                <form className="w-100" onSubmit={filter}>
                    <InputLabel>Nombres</InputLabel>
                    <TextField
                        variant="outlined"
                        className="w-100 mb-3"
                        type="text"
                        placeholder="Escribir..."
                        onChange={event => setName(event.target.value)}
                    />
                    <InputLabel>Apellidos</InputLabel>
                    <TextField
                        variant="outlined"
                        className="w-100 mb-3"
                        type="text"
                        placeholder="Escribir..."
                        onChange={event => setSurname(event.target.value)}
                    />
                    <InputLabel>Identificación (C.C)</InputLabel>
                    <TextField
                        variant="outlined"
                        className="w-100 mb-3"
                        type="number"
                        placeholder="Escribir..."
                        onChange={event => setDocument(event.target.value)}
                    />
                    <InputLabel>
                        Rol asociado
                                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={role}
                        variant="outlined"
                        onChange={event => handleRoleValue(event.target.value)}
                        label="Rol asociado"
                        className="w-100 mb-3"
                    >
                        {
                            rolesArr.map(item => (
                                <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                            ))
                        }
                    </Select>
                    <InputLabel>Estado</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={state}
                        variant="outlined"
                        onChange={event => handleStateValue(event.target.value)}
                        label="Rol asociado"
                        className="w-100 mb-3"
                    >
                        {
                            stateArr.map(item => (
                                <MenuItem key={item.id} value={item.value}>{item.state}</MenuItem>
                            ))
                        }
                    </Select>
    
                    <InputLabel>Teléfono</InputLabel>
                    <TextField
                        id="outlined-size-normal"
                        variant="outlined"
                        className="w-100 mb-3"
                        type="number"
                        placeholder="Escribir..."
                        onChange={event => setPhone(event.target.value)}
                    />
                    <InputLabel>Correo electrónico</InputLabel>
                    <TextField
                        id="outlined-size-normal"
                        variant="outlined"
                        className="w-100 mb-3"
                        type="email"
                        placeholder="Escribir..."
                        onChange={event => setEmail(event.target.value)}
                    />

                    <div className="col-12 d-flex justify-content-between px-0">
                        <button
                            type="submit"
                            className="btn btn-sm btn-hover color-5 px-3">
                            Filtrar
                        </button>
                        <button
                            onClick={resetValues}
                            type="button"
                            className="btn-linear-green px-3 btn btn-sm">
                            Limpiar
                        </button>
                    </div>
                </form>
            </CardContent>
        </Card>
    )
}

export default FilterUser
