import React from 'react';
/** Firebase */
import {db} from '../../../firebase';
/** subject */
import { userCreated, validateUserFields, userFilter } from '../../../utils/Utils';
/** styles */
import { makeStyles, Typography, Button } from '@material-ui/core';
/** table */
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
/** button */
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
/** modal */
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
/** toast */
import Snackbar from '@material-ui/core/Snackbar';
/** select */
import { Select } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
/** text fields */
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
/** spinner */
import CircularProgress from '@material-ui/core/CircularProgress';
/** skeleton */
import Skeleton from '@material-ui/lab/Skeleton';

/** styles for modal and table */
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 2),
        borderRadius: 20
    },
    borderR: {
        borderRadius: '20'
    },
    table: {
        minWidth: 300,
    },
    inputDisabled: {
        backgroundColor: '#E8E8E8'
    },
    warning: {
        color: '#ff9100'
    }
}));

const UserTable = () => {
    /** Styles */
    const classes = useStyles();
    /** state for users */
    const [uid, setUid] = React.useState('');
    /** state for users */
    const [users, setUsers] = React.useState([]);
    /** state for modal */
    const [openModal, setOpenModal] = React.useState(false);
    /** state for modal delete user */
    const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
    /** state for modal */
    const [showSpinner, setShowSpinner] = React.useState(false);
    /** state for skeleton */
    const [showSekeleton, setShowSekeleton] = React.useState(true);
    /** declare name */
    const [name, setName] = React.useState('');
    /** declare surname */
    const [surname, setSurname] = React.useState('');
    /** declare document */
    const [document, setDocument] = React.useState('');
    /** declare rol  */
    const [role, setRole] = React.useState('');
    /** declare rol  */
    const [roleName, setRoleName] = React.useState('');
    /** declare state  */
    const [state, setState] = React.useState(false);
    /** declare state  */
    const [stateName, setStateName] = React.useState('');
    /** declare state  */
    const [stateValue, setStateValue] = React.useState('');
    /** declare phone  */
    const [phone, setPhone] = React.useState('');
    /** declare email  */
    const [email, setEmail] = React.useState('');
    /** declare Roles Arr  */
    const [rolesArr, setRolesArr] = React.useState([]);
    /** declare state Arr */
    const [stateArr, setStateArr] = React.useState([]);
    /** toast state */
    const [openToast, setOpenToast] = React.useState(false);
    /** toast state */
    const [messageToast, setMessageToast] = React.useState('Usuario Actualizado con éxito!');

    
    React.useEffect(() => {
        /** get roles from firebase */
        const getAllUsers = async () => {
            try {
                const response = await db.collection('Users').get();
                const arr = response.docs.map(doc => doc.data());
                // console.log('users: ', arr);
                /** push data */
                setUsers(arr);
                setShowSekeleton(false);
            } catch (error) {
                console.log('@errorGetUsers: ', error);

            }
        };
        /** pending function of new users  */
        const observerNewUserCreated = () => {
            userCreated.subscribe((data) => {

                if (data) {
                    const newUser = data.newUser;
                    setUsers(users => [
                        ...users, newUser
                    ]);
                }
            });
        };
        /** get roles from firebase */
        const getRoles = async () => {
            try {
                const response = await db.collection('Roles').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setRolesArr(arr);
                // console.log(arr);
            } catch (error) {
                console.log('@errorGetRoles: ', error);

            }
        };
        /** get states from firebase */
        const getStates = async () => {
            try {
                const response = await db.collection('State').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setStateArr(arr);
                // console.log(arr);
            } catch (error) {
                console.log('@errorGetStates: ', error);

            }
        };
        observerNewUserCreated();
        getAllUsers();
        getRoles();
        getStates();
    },[ ]);

    // const filterUser = () => {
    //     userFilter.subscribe(async (data) => {
    //         console.log('users', users);
    //         console.log('la data para filtrar: ', data.filter);
    //         //  const response = await db.collection('Users').get();
    //         //  const arr = response.docs.map(doc => doc.data());
    //         /** re- construyyo la data que em lelga en un arreglo */
    //         let filters = (Object.entries(data.filter)).map((el) => {
    //             let obj = {};
    //             obj[el[0]] = el[1];
    //             return obj;
    //         }).filter((el) => {
    //             if (Object.values(el)[0]) { return el };
    //         });

    //         let finalUsers = [];
    //         filters.forEach(filter => {
    //             const result = search(Object.keys(filter)[0], `${filter[Object.keys(filter)[0]]}`, users);
    //             result.forEach(el => {
    //                 finalUsers.push(el);
    //             });
    //         });

    //         finalUsers = [...new Set(finalUsers)];
    //         setUsers(finalUsers);
    //         console.log("final", finalUsers);
    //     });

    // };

    // /** call function */
    // filterUser();

    const search = (key, query, elements) => {
        return elements.filter(function (el) {
            // console.log("Filtering by", key);
            const result = (el[key].toLowerCase().indexOf(query.toLowerCase()) > -1) ? el : false;
            // console.log(result)
            return result;
        })
    };



    /** función que envia la información para editarlo */
    const editUser = React.useCallback(async () => {
        try {
            setShowSpinner(true);
            const userEdit = {
                name,
                surname,
                document,
                role,
                roleName,
                state,
                stateName,
                stateValue,
                email,
                phone,
                uid,
            }
            await db.collection('Users').doc(uid).update(userEdit);
            const newUserArr = users.map(item => (
                item.uid === uid ? userEdit : item
            ));
            setUsers(newUserArr);
            /** show toas */
            handleShowToast();
            setShowSpinner(false);
            handleClose();
            /** reset values */
            resetValues();
        } catch (error) {
            console.log('@editUserError: ', error);
            setMessageToast('Ups, ha ocurrido un error');
            handleShowToast();
            setShowSpinner(false);
        }
    }, [name, surname, document, role, state, email, phone, uid, stateName, stateValue, roleName]);

    /** función que elimina al usuario seleccionado */
    const deleteUser = React.useCallback(async() =>{
        try {
            setShowSpinner(true);
            const response = await db.collection('Users').doc(uid).delete();
            /** filtramos el arreglo sin el usuario eliminado */
            const  newEditArr = users.filter(item => item.uid !== uid);
            setUsers(newEditArr);
            setMessageToast('Usuario eliminado!');
            handleShowToast();
            setShowSpinner(false);
            handleCloseDeleteMOdal();
        } catch (error) {
            console.log('@DeleteUserError: ', error);
            setMessageToast('No se ha podido borrar al usuario');
            setShowSpinner(false);
            handleShowToast();
        }
    });
    /** -------- funciones para abrir y cerrar los modales ------ */
    /** declare var for modal */
    const handleShow = (uid) => {
        /** seteamos el uid del usuario seleccionado */
        setOpenModal(true);
        patchUserInformation(uid);
    };
    /** cierra el modal de actualizar usuarios */
    const handleClose = () => setOpenModal(false);
    /** declare var for modal */
    const handleShowDeleteModal = (uid, name, surname) => {
        /** seteamos el uid del usuario seleccionado */
        setUid(uid);
        setName(name);
        setSurname(surname);
        setOpenDeleteModal(true);
    };
    /** cierra el modal de eliminar usuarios */
    const handleCloseDeleteMOdal = () => {
        setName('');
        setUid('');
        setSurname('');
        setOpenDeleteModal(false);
    }; 
    /** -------- funciones para abrir y cerrar los modales ------ */


    /** process edit onsubmit form */
    const editProcessData = (event) => {
        event.preventDefault();
        /** validamos que los campos esten llenos */
        const resValidators = validateUserFields({ name, surname, document, role, stateValue, phone, email });
        if (!resValidators.state) {
            setMessageToast(resValidators.msg);
            handleShowToast();
            return
        }
        /** validamos que los campos esten llenos */
        editUser();
    };
    /** setea la información del usuario seleccionado */
    const patchUserInformation = (id) =>{
        setUid(id);
        const userSelected = users.filter(user => user.uid === id);
        setName(userSelected[0].name);
        setSurname(userSelected[0].surname);
        setDocument(userSelected[0].document);
        setRole(userSelected[0].role);
        setState(userSelected[0].state);
        setStateName(userSelected[0].stateName);
        setStateValue(userSelected[0].stateValue);
        setPhone(userSelected[0].phone);
        setEmail(userSelected[0].email);
    };
    /** setRole value */
    const handleRoleValue = event => {
        const filterRole = rolesArr.filter(item => item.value == event);
        setRoleName(filterRole[0].name);
        setRole(event);
    }
    /** set state value */
    const handleStateValue = event => {
        const filterState = stateArr.filter(item => item.value == event);
        setStateValue(event);
        setStateName(filterState[0].name);
        setState(filterState[0].state);
    }
    /** declare var for toast */
    const handleShowToast = () => setOpenToast(true);
    const handleCloseToast = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenToast(false);
    };

    /** reset form values */
    const resetValues = () => {
        setName('');
        setSurname('');
        setDocument('');
        setRole('');
        setRoleName('');
        setState(false);
        setStateName('');
        setStateValue('');
        setPhone('');
        setEmail('');
    };

    return (
        <div className="w-100">
            {
                showSekeleton ? (
                    <div className="p-4">
                        <Skeleton height={40} />
                        <Skeleton animation={false}  height={20}  />
                        <Skeleton animation="wave" className="mb-3" height={30}  />
                        <Skeleton />
                        <Skeleton animation={false}  height={20}  />
                        <Skeleton animation="wave" className="mb-3" height={30}  />
                        <Skeleton />
                        <Skeleton animation={false}  height={20}  />
                        <Skeleton animation="wave" className="mb-3" height={30}  />
                        <Skeleton />
                        <Skeleton animation={false}  height={20}  />
                        <Skeleton animation="wave" className="mb-3" height={30}  />
                    </div> 
                ) : (
                        <TableContainer className="w-100" >
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>
                                            <Typography
                                                variant="h6">
                                                Nombres
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Apellidos
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Identificación
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Rol
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Estado
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Teléfono
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Email
                                </Typography>
                                        </TableCell>
                                        <TableCell align="center">
                                            <Typography
                                                variant="h6">
                                                Acción
                                </Typography>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {users.map((user) => (
                                        <TableRow key={user.uid}>
                                            <TableCell component="th" scope="row">
                                                {user.name}
                                            </TableCell>
                                            <TableCell align="center">{user.surname}</TableCell>
                                            <TableCell align="center">{user.document}</TableCell>
                                            <TableCell align="center">{user.roleName}</TableCell>
                                            <TableCell align="center">{user.stateName}</TableCell>
                                            <TableCell align="center">{user.phone}</TableCell>
                                            <TableCell align="center">{user.email}</TableCell>
                                            <TableCell align="center">
                                                <IconButton
                                                    className="pr-0"
                                                    style={{ color: '#4a4a4a' }}
                                                    onClick={() => handleShow(user.uid)}>
                                                    <CreateIcon />
                                                </IconButton>
                                                <IconButton
                                                    className="pr-0 mr-3"
                                                    style={{ color: '#4a4a4a' }}
                                                    onClick={() => handleShowDeleteModal(user.uid, user.name, user.surname)}>
                                                    <DeleteIcon />
                                                </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                )
            }
    
            {/* Modal containter for edit user */}
            <Modal
                className={classes.borderR}
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableAutoFocus={true}
                disableEnforceFocus={true}>
                <Fade in={openModal}>
                    <div className={classes.paper}>
                        <div className="container p-4">
                            <Typography variant="h5"
                                className="mb-4 " color="primary">Editar usuario</Typography>
                            <div className="row justify-content-center">
                                <form onSubmit={editProcessData}
                                    className="px-4 col-10">
                                    <div className="row">
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Nombres</InputLabel>
                                            <TextField
                                                value={name}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="text"
                                                placeholder="Escribir..."
                                                onChange={event => setName(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Apellidos</InputLabel>
                                            <TextField
                                                value={surname}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="text"
                                                placeholder="Escribir..."
                                                onChange={event => setSurname(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Identificación</InputLabel>
                                            <TextField
                                                value={document}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="number"
                                                placeholder="Escribir..."
                                                onChange={event => setDocument(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Rol asociado</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-outlined-label"
                                                id="demo-simple-select-outlined"
                                                value={role}
                                                variant="outlined"
                                                onChange={event => handleRoleValue(event.target.value)}
                                                label="Rol asociado"
                                                className="w-100 mb-3">
                                                {
                                                    rolesArr.map(item => (
                                                        <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Estado</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-outlined-label"
                                                id="demo-simple-select-outlined"
                                                value={stateValue}
                                                variant="outlined"
                                                onChange={event => handleStateValue(event.target.value)}
                                                label="Rol asociado"
                                                className="w-100 mb-3">
                                                {
                                                    stateArr.map(item => (
                                                        <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                            <InputLabel>Teléfono</InputLabel>
                                            <TextField
                                                value={phone}
                                                variant="outlined"
                                                className="w-100 mb-3"
                                                type="number"
                                                placeholder="Escribir..."
                                                onChange={event => setPhone(event.target.value)}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6 mb-4">
                                            <InputLabel>Correo electrónico</InputLabel>
                                            <TextField
                                                disabled
                                                value={email}
                                                variant="outlined"
                                                className={`${classes.inputDisabled} w-100 mb-3`}
                                                type="email"
                                                placeholder="Escribir..."
                                            />
                                        </div>
                                        <div className="col-12 d-flex justify-content-center px-0">
                                            {
                                                showSpinner ? (
                                                    <CircularProgress color="primary" />
                                                ) : (
                                                <button
                                                    type="submit"
                                                    className="btn btn-sm btn-hover color-5 px-4">
                                                    Actualizar
                                                </button>
                                                )
                                            }
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Fade>
            </Modal>

            {/* Modal for delete user */}
            <Modal
                className={classes.borderR}
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openDeleteModal}
                onClose={handleCloseDeleteMOdal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableAutoFocus={true}
                disableEnforceFocus={true}>

                <Fade in={openDeleteModal}>
                    <div className={classes.paper}>
                        <div className="container p-4 text-center">
                            {
                                showSpinner ? (
                                    <CircularProgress color="primary" />
                                ) : (
                                        <WarningIcon
                                            className="mb-3"
                                            style={{ color: '#ff9100', fontSize: 100 }}
                                        />
                                    )
                            }
                            <h3 
                                className={`${classes.warning} text-center mb-4`}>
                                    ¡Ups ten cuidado!
                                </h3>
                            <p>¿Estas seguro de eliminar al usuario {name} {surname}?</p>

                            <div className="row">
                                <div className="col-6 text-center">
                                    <button 
                                        className="btn btn-lg btn-hover color-11"
                                        onClick={deleteUser}>
                                        Eliminar
                                    </button>
                                </div>
                                <div className="col-6 text-center">
                                    <button 
                                        className="btn btn-lg btn-hover color-9"
                                        onClick={handleCloseDeleteMOdal}>
                                        Cancelar
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </Fade>
            </Modal>
            {/* Toast */}
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={openToast}
                autoHideDuration={6000}
                onClose={handleCloseToast}
                message={messageToast}
                action={
                    <React.Fragment>
                        <Button color="secondary" size="small" onClick={handleCloseToast}>
                            Cerrar
                        </Button>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseToast}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </div>
    )
}

export default UserTable
