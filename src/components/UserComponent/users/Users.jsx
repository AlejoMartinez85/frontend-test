/** core */
import React from 'react';
/** Cards */
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
/** css */
import {stylesCss} from '../../../styles.css';
/** components */
import FilterUser from '../filterUser/FilterUser';
import UserTable from '../userTable/UserTable';
import CreateUser from '../createUser/CreateUser';


const Users = () => {
    return (
        <div className="w-100">
        {/* <div className="container"> */}
            <div className="row">
                <div className="col-md-7 col-xs-12 col-xl-9 px-0 mb-4">
                    <Card>
                        <CardContent>
                            {/* create user component */}
                            <CreateUser/>
                            <div className="mb-4"></div>
                            {/* tableUser Component */}
                            <UserTable/>
                        </CardContent>
                    </Card>
                </div>
                <div className="col-md-5  col-xs-12 col-xl-3">
                    {/* Filter user component */}
                    <FilterUser/> 
                </div>
            </div>
        </div>
    )
}

export default Users
