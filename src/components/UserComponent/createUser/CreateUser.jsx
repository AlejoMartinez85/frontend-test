import React from 'react';
/** icon */
import GroupIcon from '@material-ui/icons/Group';
/** styles */
import { Typography, makeStyles, Button} from '@material-ui/core';
/** text fields */
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
/** css */
import { stylesCss } from '../../../styles.css';
/** select */
import { Select } from '@material-ui/core';
/** icon */
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
/** Firebase */
import { auth, db } from '../../../firebase';
/** modal */
// import Modal from 'react-bootstrap/Modal';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
/** subject */
import { userCreated, validateUserFields } from '../../../utils/Utils';
/** toast */
import Snackbar from '@material-ui/core/Snackbar';


/** styles for modal */
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 2),
        borderRadius:20
    },
    borderR:{
        borderRadius:'20'
    }
}));

const CreateUser = () => {
    /** asign modal classes */
    const classes = useStyles();
    /** state for modal */
    const [openModal, setOpenModal] = React.useState(false);
    /** declare name */
    const [name, setName] = React.useState('');
    /** declare surname */
    const [surname, setSurname] = React.useState('');
    /** declare document */
    const [document, setDocument] = React.useState('');
    /** declare rol  */
    const [role, setRole] = React.useState('');
    /** declare rol  */
    const [roleName, setRoleName] = React.useState('');
    /** declare state  */
    const [state, setState] = React.useState(false);
    /** declare state  */
    const [stateName, setStateName] = React.useState('');
    /** declare state  */
    const [stateValue, setStateValue] = React.useState('');
    /** declare password  */
    const [password, setPassword] = React.useState('');
    /** show password state */
    const [showPassword, setShowPassword] = React.useState(false);
    /** changeInputType */
    const [inputType, setInputType] = React.useState('password');
    /** declare phone  */
    const [phone, setPhone] = React.useState('');
    /** declare email  */
    const [email, setEmail] = React.useState('');
    /** declare Roles Arr  */
    const [rolesArr, setRolesArr] = React.useState([]);
    /** declare state Arr */
    const [stateArr, setStateArr] = React.useState([]);
    /** toast state */
    const [openToast,setOpenToast]= React.useState(false);
    /** toast state */
    const [messageToast, setMessageToast]= React.useState('Usuario creado con éxito!');

    /** declare var for states */
    const handleClose = () => setOpenModal(false);
    const handleShow = () => setOpenModal(true);
    /** declare var for states */
    const handleShowToast = () => setOpenToast(true);
    const handleCloseToast = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenToast(false);
    };

    /** onInit */
    React.useEffect(() => {
        /** get roles from firebase */
        const getRoles = async () => {
            try {
                const response = await db.collection('Roles').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setRolesArr(arr);
                // console.log(arr);
            } catch (error) {
                console.log('@errorGetRoles: ', error);
            }
        };
        /** get states from firebase */
        const getStates = async () => {
            try {
                const response = await db.collection('State').get();
                const arr = response.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                /** push data */
                setStateArr(arr);
                // console.log('states: ',arr);
            } catch (error) {
                console.log('@errorGetStates: ', error);

            }
        };
        /** call function */
        getRoles();
        getStates();
    }, []);

    /** process register */
    const processRegister = (event) => {
        event.preventDefault();
        /** validamos que los campos esten llenos */
        const resValidators = validateUserFields({ name, surname, document, role, stateValue, password, phone, email });
        if (!resValidators.state) {
            setMessageToast(resValidators.msg);
            handleShowToast();
            return
        }
        register();
    };
    /** function register */
    const register = React.useCallback(async () => {
        try {
            const response = await auth.createUserWithEmailAndPassword(email, password);
            // console.log('@registerResponse:', response.user);
            /** relacionamos el uid del registro a la colección de 
             * usuarios
             */
            const newUser = {
                name,
                surname,
                document,
                role,
                roleName,
                phone,
                email: response.user.email,
                uid: response.user.uid,
                state,
                stateName,
                stateValue
            };
            await db.collection('Users').doc(response.user.uid).set(newUser);
            /** emitt nuew user to userTable component */
            userCreated.next({
                newUser
            });
            /** show toas */
            handleShowToast();
            /** reset inputs values */
            resetValues();
        } catch (error) {
            console.error('@errorRegister,', error);
            setMessageToast('Ups, ha ocurrido un error');
            handleShowToast();
        }
    }, [name, surname, document, role, state, password, phone, email, stateName, stateValue, roleName]);
    /** reset form values */
    const resetValues = () => {
        setName('');
        setSurname('');
        setDocument('');
        setRole('');
        setRoleName('');
        setState(false);
        setStateName('');
        setStateValue('');
        setPassword('');
        setPhone('');
        setEmail('');
    };

    /** change showPassword state */
    const showValuePassword = () => {
        /** change showPassword value */
        setShowPassword(!showPassword);
        /** is validated to know what kind of input to put */
        if (!showPassword) {
            setInputType('text');
        } else {
            setInputType('password');
        }
    };
    /** evita que el formulario recargue la página */
    const handleMouseDownPassword = (event) => {
        // event.preventDefault();
    };

    /** setRole value */
    const handleRoleValue = event => {
        const filterRole = rolesArr.filter(item => item.value == event);
        setRoleName(filterRole[0].name);
        setRole(event);
    }
    /** set state value */
    const handleStateValue = event => {
        const filterState = stateArr.filter(item  => item.value == event);
        setStateValue(event);
        setStateName(filterState[0].name);
        setState(filterState[0].state);
    }


    return (
        <di className="row">
            {/* title and create user button */}
            <div className="col-12">
                <div className="row">
                    <div className="col-8 col-sm-8 col-xs-8 col-md-8 col-lx-8">
                        <div className="d-flex">
                            <GroupIcon
                                className="mr-2"
                                color="primary"
                                style={{ fontSize: 30 }} />
                            <Typography 
                                variant="h6"
                                color="primary">
                                Usuarios existentes
                            </Typography>
                        </div>
                    </div>
                    {/* button for create user */}
                    <div 
                        className="col-4 col-sm-4 col-xs-4 col-md-4 col-lx-4">
                        <div className="text-right">
                            <button
                                className="btn btn-hover color-9"
                                onClick={handleShow}>
                                Crear Usuario
                            </button>
                            {/* modal component */}
                            <Modal
                                className={classes.borderR}
                                aria-labelledby="transition-modal-title"
                                aria-describedby="transition-modal-description"
                                className={classes.modal}
                                open={openModal}
                                onClose={handleClose}
                                closeAfterTransition
                                BackdropComponent={Backdrop}
                                BackdropProps={{
                                    timeout: 500,
                                }}
                                disableAutoFocus={true}
                                disableEnforceFocus={true}
                            >
                                <Fade in={openModal}>
                                    <div className={classes.paper}>
                                        <div className="container p-4">
                                            <Typography variant="h5"
                                                className="mb-4" color="primary">Crear usuario</Typography>
                                            <div className="row justify-content-center">
                                                <form onSubmit={processRegister}
                                                    className="px-4 col-10">
                                                    <div className="row justify-content-center">
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Nombres</InputLabel>
                                                            <TextField
                                                                value={name}
                                                                variant="outlined"
                                                                className="w-100 mb-3"
                                                                type="text"
                                                                placeholder="Escribir..."
                                                                onChange={event => setName(event.target.value)}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Apellidos</InputLabel>
                                                            <TextField
                                                                value={surname}
                                                                variant="outlined"
                                                                className="w-100 mb-3"
                                                                type="text"
                                                                placeholder="Escribir..."
                                                                onChange={event => setSurname(event.target.value)}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Identificación</InputLabel>
                                                            <TextField
                                                                value={document}
                                                                variant="outlined"
                                                                className="w-100 mb-3"
                                                                type="number"
                                                                placeholder="Escribir..."
                                                                onChange={event => setDocument(event.target.value)}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Rol asociado</InputLabel>
                                                            <Select
                                                                labelId="demo-simple-select-outlined-label"
                                                                id="demo-simple-select-outlined"
                                                                value={role}
                                                                variant="outlined"
                                                                onChange={event => handleRoleValue(event.target.value)}
                                                                label="Rol asociado"
                                                                className="w-100 mb-3"
                                                            >
                                                                {
                                                                    rolesArr.map(item => (
                                                                        <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                                                                    ))
                                                                }
                                                            </Select>
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Estado</InputLabel>
                                                            <Select
                                                                labelId="demo-simple-select-outlined-label"
                                                                id="demo-simple-select-outlined"
                                                                value={stateValue}
                                                                variant="outlined"
                                                                onChange={event => handleStateValue(event.target.value)}
                                                                label="Rol asociado"
                                                                className="w-100 mb-3"
                                                            >
                                                                {
                                                                    stateArr.map(item => (
                                                                        <MenuItem key={item.id} value={item.value}>{item.name}</MenuItem>
                                                                    ))
                                                                }
                                                            </Select>
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Contraseña</InputLabel>
                                                            <TextField
                                                                className="w-100"
                                                                placeholder="Contraseña"
                                                                variant="outlined"
                                                                type={inputType}
                                                                onChange={event => setPassword(event.target.value)}
                                                                value={password}
                                                                InputProps={{
                                                                    endAdornment: (
                                                                        <InputAdornment position="start">
                                                                            <IconButton
                                                                                className="pr-0"
                                                                                style={{ color: '#4a4a4a' }}
                                                                                onMouseDown={() => handleMouseDownPassword()}
                                                                                onClick={() => showValuePassword()}>
                                                                                {
                                                                                    showPassword ? (<Visibility />) : (<VisibilityOff />)
                                                                                }
                                                                            </IconButton>
                                                                        </InputAdornment>
                                                                    )
                                                                }}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6">
                                                            <InputLabel>Teléfono</InputLabel>
                                                            <TextField
                                                                value={phone}
                                                                variant="outlined"
                                                                className="w-100 mb-3"
                                                                type="number"
                                                                placeholder="Escribir..."
                                                                onChange={event => setPhone(event.target.value)}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-xs-12 col-md-6 col-lx-6 mb-4">
                                                            <InputLabel>Correo electrónico</InputLabel>
                                                            <TextField
                                                                value={email}
                                                                variant="outlined"
                                                                className="w-100 mb-3"
                                                                type="email"
                                                                placeholder="Escribir..."
                                                                onChange={event => setEmail(event.target.value)}
                                                            />
                                                        </div>
                                                        <div className="col-8 d-flex justify-content-around px-0">
                                                            <button
                                                                type="submit"
                                                                className="btn btn-sm btn-hover color-5 px-4">
                                                                Crear
                                                            </button>
                                                            <button
                                                                type="button"
                                                                onClick={() => resetValues()}
                                                                className="btn-linear-green px-4 btn btn-sm">
                                                                Limpiar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </Fade>
                            </Modal>
                        </div>
                    </div>
                </div>
            </div>
            {/* Toast */}
            <Snackbar              
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={openToast}
                autoHideDuration={6000}
                onClose={handleCloseToast}
                message={messageToast}
                action={
                    <React.Fragment>
                        <Button color="secondary" size="small" onClick={handleCloseToast}>
                            Cerrar
                        </Button>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseToast}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        </di>
    )
}

export default CreateUser
